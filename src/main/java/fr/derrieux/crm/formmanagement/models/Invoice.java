package fr.derrieux.crm.formmanagement.models;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

public class Invoice {
  @Id @GeneratedValue(strategy = GenerationType.AUTO) 
  private Long id;

  @Column(length = 255, nullable = false)
  private String reference;

  @Column(length = 255, nullable = false)
  private Float value;

  @Column(length = 255, nullable = false)
  private LocalDateTime due_date;


}
