package fr.derrieux.crm.formmanagement.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "customer")

public class Customer {

    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(length = 99, nullable = false)
    private String first_name;

    @Column(length = 99, nullable = false)
    private String last_name;

    @Column(length = 320, nullable = false)
    private String email;

    @Column(length = 10, nullable = false)
    private String phone_number;

    @Column(length = 200, nullable = false)
    private String address;

    @Column(length = 99, nullable = false)
    private String society;
}