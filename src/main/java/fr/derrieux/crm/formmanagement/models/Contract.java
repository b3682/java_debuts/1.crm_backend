package fr.derrieux.crm.formmanagement.models;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Data
@Table(name = "contract")

public class Contract {
  @Id 
  @GeneratedValue(strategy = GenerationType.IDENTITY) 
  private Long id;

  @Column(length = 255, nullable = false)
  private String reference;

  @Column(length = 255, nullable = false)
  private String subscribed;

  @Column(length = 255, nullable = false)
  private Float value;

  @Column(length = 255, nullable = false)
  private LocalDateTime start_date;

  @Column(length = 255, nullable = false)
  private LocalDateTime end_date;
}
