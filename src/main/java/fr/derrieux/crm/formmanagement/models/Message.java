package fr.derrieux.crm.formmanagement.models;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

public class Message {
  @Id @GeneratedValue(strategy = GenerationType.AUTO) 
  private Long id;

  @Column(length = 255, nullable = false)
  private LocalDateTime sent_date;

  @Column(length = 255, nullable = false)
  private String subject;

  @Column(length = 255, nullable = false)
  private String context;


}
