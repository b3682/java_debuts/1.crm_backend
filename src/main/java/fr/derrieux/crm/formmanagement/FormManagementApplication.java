package fr.derrieux.crm.formmanagement;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableJpaRepositories
public class FormManagementApplication {

	public static void main(String[] args) {
		SpringApplication.run(FormManagementApplication.class, args);
	}

}
