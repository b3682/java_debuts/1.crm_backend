package fr.derrieux.crm.formmanagement.repositories;

import org.springframework.data.repository.CrudRepository;

import fr.derrieux.crm.formmanagement.models.MicroService;

public interface MicroServiceRepository extends CrudRepository<MicroService, Long> {
    
}
