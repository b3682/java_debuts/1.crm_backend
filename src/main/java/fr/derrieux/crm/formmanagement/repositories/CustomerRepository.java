package fr.derrieux.crm.formmanagement.repositories;


import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import fr.derrieux.crm.formmanagement.models.Customer;

@Repository
public interface CustomerRepository extends CrudRepository <Customer, Long> {

    @Override
     Iterable<Customer> findAll();
        
        
    
    

}
